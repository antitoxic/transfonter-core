# @font-face generator

Modern and simple css @font-face generator and subsetter. Based on https://github.com/zoltan-dulac/css3FontConverter

This component used on http://transfonter.org

Used software:

* [FontForge](http://fontforge.github.io/)
* [ttf2eot](https://code.google.com/p/ttf2eot/)
* [sfnt2woff](http://people.mozilla.org/~jkew/woff/)
* [woff2_compress](https://github.com/google/woff2)
* [ttfautohint](http://www.freetype.org/ttfautohint/)
* [pyftsubset](https://github.com/behdad/fonttools)
<?php
namespace Fatum12\TransfonterCore;

use Fatum12\TransfonterCore\Exception\ArgumentException;
use Fatum12\TransfonterCore\Util\Template;

class FontManager
{
	/**
	 * @var Config
	 */
	protected $options;
	/**
	 * @var array Source files
	 */
	protected $files = [];

	public function __construct(array $options = [])
	{
		$this->options = new Config(array_replace([
			'stylesheetName' => 'stylesheet.css',
			'demoName' => 'demo.html',
			'demoLanguage' => 'en',
			'formats' => [Font::TYPE_WOFF, Font::TYPE_EOT, Font::TYPE_TTF],
			'subsets' => [],
			'autohint' => false,
			'compressSvg' => false,
			// add local rule
			'local' => false,
			// embed font in CSS
			'base64' => false,
			// family support in CSS
			'fontFamily' => true,
		], $options));
	}

	public function add($path)
	{
		$this->files[] = $path;
	}

	public function loadFromDir($dir)
	{
		if (!is_dir($dir)) {
			throw new ArgumentException("Wrong directory: {$dir}");
		}
		$dir = rtrim($dir, '/\\');

		foreach (glob($dir . '/*.{ttf,otf}', \GLOB_BRACE) as $file) {
			$this->files[] = $file;
		}
	}

	public function process($dest)
	{
		if (!is_writable($dest)) {
			throw new ArgumentException("Directory {$dest} is not writable");
		}
		if ($this->options->get('demoLanguage') == 'ru') {
			$demoLetters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя <br />
				АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ <br />
				abcdefghijklmnopqrstuvwxyz <br />
				ABCDEFGHIJKLMNOPQRSTUVWXYZ <br />';
			$demoString = 'Съешь же ещё этих мягких французских булок, да выпей чаю.';
		} else {
			$demoLetters = 'abcdefghijklmnopqrstuvwxyz <br />
				ABCDEFGHIJKLMNOPQRSTUVWXYZ <br />';
			$demoString = 'The quick brown fox jumps over the lazy dog.';
		}

		$cssFile = fopen($dest . '/' . $this->options->get('stylesheetName'), 'w');
		fwrite($cssFile, "/* This stylesheet generated by Transfonter (http://transfonter.org) on " . gmdate('F j, Y g:i A') . " */\n");
		$demoStyles = [];
		$demoTexts = [];
		$converter = new FontConverter($this->options);
		foreach ($this->files as $index => $file) {
			$font = new Font($file);
			$converter->convert($font, $dest);
			// write css rules
			fwrite($cssFile, "\n");
			fwrite($cssFile, $converter->getCSS());
			$demoStyles[] = Template::render('demo_style', [
				'index' => $index,
				'fontName' => $this->options->get('fontFamily') ? $font->getFamilyName() : $font->getName(),
				'weight' => $this->options->get('fontFamily') ? $font->getWeight() : 'normal',
				'style' => $this->options->get('fontFamily') ? $font->getStyle() : 'normal'
			]);
			$demoTexts[] = Template::render('demo_item', [
				'index' => $index,
				'fontName' => $font->getFullName(),
				'letters' => $demoLetters,
				'string' => $demoString
			]);
		}

		fclose($cssFile);
		// write demo file
		file_put_contents($dest . '/' . $this->options->get('demoName'), Template::render('demo', [
			'stylesheet' => $this->options->get('stylesheetName'),
			'styles' => implode("\n", $demoStyles),
			'text' => implode("\n", $demoTexts),
		]));
	}
}